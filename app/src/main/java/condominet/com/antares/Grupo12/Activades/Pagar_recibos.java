package condominet.com.antares.Grupo12.Activades;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import condominet.com.antares.Grupo12.Adaptadores.AdaptadorRecibos;
import condominet.com.antares.Grupo12.ConstantesURL;
import condominet.com.antares.Grupo12.Modoles.Bancos;
import condominet.com.antares.Grupo12.Modoles.Recibos;
import condominet.com.antares.R;
import condominet.com.antares.RecyclerItemClickListener;

public class Pagar_recibos extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    String JsonURLParaLosRecibos ;
    RequestQueue requestQueue;
    private RecyclerView recycler_recibos;
    private RelativeLayout relativeLayout_sin_recibos;
    private RelativeLayout rl_reciclador_tipos_pago;
    private RelativeLayout rl_reciclador_fb;
    private TextView montoTotal;
    private AdaptadorRecibos adapter_recibos;
    private RecyclerView.LayoutManager lManager_recibos;
    private int monto_total_pagar  ;
    private FloatingActionButton fb_pagar;

    private NotificationCompat.Builder builder;
    private NotificationManager notificationManager;
    private int notification_id;
    private RemoteViews remoteViews;
    private  Context context;
    static String stringFechaParaRegistrar ;
    static EditText et_fecha;
    int id_vivienda, id_junta;
    String id_recibo;
    TextView tv_monto_texto;
    TextView tv_recibo;
    TextView tv_id_vivienda;
    EditText et_numero_confirmacion;
    Button btn_cnfirmar ;
    JSONObject jsonParams ;
    String tipoCheqeEfectivo, nro_confirmacion, banco_emisor, banco_receptor_;
    Spinner tipoPago, bancoEmisor, banco_receptor;

    String urlBancos ,urlPagar;

    AdapterView.OnItemSelectedListener adaptadorSpinner ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagar_recibos);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recycler_recibos = (RecyclerView) findViewById(R.id.reciclador_recibos_pendientes);
        montoTotal       = (TextView)     findViewById(R.id.tv_monto_total);
        relativeLayout_sin_recibos = (RelativeLayout) findViewById(R.id.rl_sinPagos);
        rl_reciclador_tipos_pago = (RelativeLayout) findViewById(R.id.rl_reciclador_tipos_pago);
        rl_reciclador_fb = (RelativeLayout) findViewById(R.id.rl_reciclador_fb);
        fb_pagar          = (FloatingActionButton) findViewById(R.id.fb_recibos);
        et_fecha          = (EditText) findViewById(R.id.et_su_fecha_nacimiento);
        et_numero_confirmacion          = (EditText) findViewById(R.id.et_numero_confirmacion);
        btn_cnfirmar          = (Button) findViewById(R.id.btn_cnfirmar);

        tv_monto_texto          = (TextView) findViewById(R.id.tv_monto_texto_real);
        tv_recibo          = (TextView) findViewById(R.id.tv_recibo);
        tv_id_vivienda          = (TextView) findViewById(R.id.tv_id_vivienda);

        relativeLayout_sin_recibos.setVisibility(View.GONE);
        recycler_recibos.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager_recibos = new LinearLayoutManager(getApplicationContext());
        recycler_recibos.setLayoutManager(lManager_recibos);

        JsonURLParaLosRecibos = ConstantesURL.JsonURLParaLosRecibos;
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        adaptadorSpinner = this ;


                // Spinner 1
                tipoPago = (Spinner) findViewById(R.id.sp_tipo_pago);
                tipoPago.setOnItemSelectedListener(this);
                // Create an ArrayAdapter using the string array and a default spinner layout
                ArrayAdapter<CharSequence> adapter =
                        ArrayAdapter.createFromResource(this, R.array.arreglo_tipo_pago,R.layout.spinner_item);
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                tipoPago.setAdapter(adapter);
                tipoPago.setOnItemSelectedListener(this);


                bancoEmisor = (Spinner) findViewById(R.id.sp_banco_emisor);
                bancoEmisor.setOnItemSelectedListener(this);
                // Create an ArrayAdapter using the string array and a default spinner layout
                ArrayAdapter<CharSequence> adapterBanco =
                        ArrayAdapter.createFromResource(this, R.array.arreglo_emisor,R.layout.spinner_item);
                // Specify the layout to use when the list of choices appears
                adapterBanco.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                bancoEmisor.setAdapter(adapterBanco);
                bancoEmisor.setOnItemSelectedListener(this);


        btn_cnfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Condificiones para pagar
                requestPostPagar();
            }
        });


        System.out.println("JsonURLParaLosRecibos Recibos"+JsonURLParaLosRecibos);
        Intent intent = getIntent();
        id_vivienda= intent.getIntExtra("id_vivienda",0);
        id_junta= intent.getIntExtra("id_junta",0);

        urlBancos  = "http://"+ConstantesURL.iP+":3000/api/v1/b_accounts/"+id_junta;
        urlPagar  = "http://"+ConstantesURL.iP+":3000/junta/pago_propietarios";
        System.out.println("JsonURLParaLa Junta"+id_junta);
        System.out.println("JsonURLParaLa Junta 2"+urlBancos);

        verBancos();

        verRecibosPendientes();


        monto_total_pagar = 0 ;

//       datosReciboCableado();


        fb_pagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                System.out.println(" Monto "+monto_total_pagar);
                System.out.println(" id_vivienda "+id_vivienda);
                System.out.println(" id_recibo "+id_recibo);


                tv_monto_texto.setText(""+monto_total_pagar);
                tv_recibo.setText(""+id_vivienda);
                tv_id_vivienda.setText(""+id_recibo);

                rl_reciclador_tipos_pago.setVisibility(View.VISIBLE);
                rl_reciclador_fb.setVisibility(View.GONE);
            }
        });



        recycler_recibos.addOnItemTouchListener(
                new RecyclerItemClickListener(getBaseContext(), recycler_recibos ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {



                        monto_total_pagar = monto_total_pagar +  adapter_recibos.getMontoMes(position);
                        id_recibo =  adapter_recibos.getIdRecibo(position)+"";
                        montoTotal.setText("Total: "+monto_total_pagar+" Bs.");
                        //  Toast.makeText(getBaseContext(), "Single Click on position :"+position, Toast.LENGTH_SHORT).show();
                        //  Toast.makeText(getBaseContext(), "Monto :"+monto_total_pagar, Toast.LENGTH_SHORT).show();
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );


    }



    public void requestPostPagar(){

        /*
        {
            "junta_pago_propietario":{

            "tipo" : "Cheque",
                    "banco" : "Bicentenario Banco",
                    "numero" : "12121223122" ,
                    "administradora_vivienda_id" : 2,
                    "junta_b_account_id" : 2,
                    "administradora_recibo_id" : 1,
                    "monto": 11111
        }
        }*/

        jsonParams = new JSONObject();
        JSONObject manJson = new JSONObject();
        try {
            manJson.put("tipo",tipoCheqeEfectivo);
            manJson.put("banco",banco_emisor);
            manJson.put("numero",nro_confirmacion);
            manJson.put("administradora_vivienda_id",id_vivienda);
            manJson.put("junta_b_account_id",id_junta);
            manJson.put("administradora_recibo_id",id_recibo);
            manJson.put("monto",monto_total_pagar);
            jsonParams.put("junta_pago_propietario",manJson);
        } catch (JSONException e) {
            e.printStackTrace();

        }

        System.out.println("Json a pasar "+jsonParams.toString());


        JsonObjectRequest postRequest = new JsonObjectRequest( Request.Method.POST, urlPagar,
                jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println("Repuesta de post "+response);
                        Toast.makeText(getBaseContext(), "Pago exitoso:", Toast.LENGTH_SHORT).show();

                        verRecibosPendientes();
                        rl_reciclador_tipos_pago.setVisibility(View.GONE);
                        rl_reciclador_fb.setVisibility(View.VISIBLE);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Handle Error
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };


        requestQueue.add(postRequest);


        System.out.println("Ver recibos final ");

    }






    public void verBancos(){

        System.out.println("Ver bancos");


        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, urlBancos,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        System.out.println("Ver bancos respuesta");

                        Gson gson = new GsonBuilder().disableHtmlEscaping().serializeNulls().create();
                        ArrayList<Bancos> propietarioBancos = null;

                        try {
                            Type listType = new TypeToken<ArrayList<Bancos>>(){}.getType();

                            propietarioBancos = gson.fromJson(response.toString(), listType);

                            ArrayList<String> bancoNombre = null;
                            bancoNombre= new ArrayList<>();

                            for (int i = 0; i < propietarioBancos.size(); i++) {

                                bancoNombre.add(propietarioBancos.get(i).getBanco());

                            }


//                            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, contactlist);


                            // Specify the layout to use when the list of choices appears

                            banco_receptor = (Spinner) findViewById(R.id.sp_banco_receptor);
                            banco_receptor.setOnItemSelectedListener(adaptadorSpinner);
                            // Create an ArrayAdapter using the string array and a default spinner layout
                          //                            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, contactlist);

                           // ArrayAdapter<String>  adapterBancoReceptor=
                             //       ArrayAdapter.createFromResource(getBaseContext(),bancoNombre ,R.layout.spinner_item);
                            ArrayAdapter<String> adapterBancoReceptor = new ArrayAdapter(getBaseContext(),
                                    R.layout.spinner_item, bancoNombre);

                            // Specify the layout to use when the list of choices appears
                            adapterBancoReceptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            // Apply the adapter to the spinner
                            banco_receptor.setAdapter(adapterBancoReceptor);
                            banco_receptor.setOnItemSelectedListener(adaptadorSpinner);

                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }

                    }





                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error.Response", "" + error);
                        Toast.makeText(getBaseContext(), "Error.Response recibos :"+error, Toast.LENGTH_SHORT).show();

                    }
                }
        );

        requestQueue.add(getRequest);


        System.out.println("Ver recibos final ");

    }
    public void verRecibosPendientes(){

        System.out.println("Ver recibos");


        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, JsonURLParaLosRecibos,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        Gson gson = new GsonBuilder().disableHtmlEscaping().serializeNulls().create();
                        ArrayList<Recibos> propietarioRecibos = null;

                        try {
                            Type listType = new TypeToken<ArrayList<Recibos>>(){}.getType();

                            propietarioRecibos = gson.fromJson(response.toString(), listType);


                            for (int i = 0; i < propietarioRecibos.size(); i++) {

                                propietarioRecibos.get(i).setSeleccionado(false);
                                System.out.println("Resultados recibos monto "+propietarioRecibos.get(i).getMonto());

                            }

                            adapter_recibos = new AdaptadorRecibos(propietarioRecibos);

                            recycler_recibos.setAdapter(adapter_recibos);

                            if (propietarioRecibos.size() == 0 ){

                                relativeLayout_sin_recibos.setVisibility(View.VISIBLE);
                                rl_reciclador_fb.setVisibility(View.GONE);
                            }
                            else{
                                relativeLayout_sin_recibos.setVisibility(View.GONE);
                                rl_reciclador_fb.setVisibility(View.VISIBLE);

                            }

                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Resultados recibos"+ propietarioRecibos.size());



                    }





                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error.Response", "" + error);
                        Toast.makeText(getBaseContext(), "Error.Response recibos :"+error, Toast.LENGTH_SHORT).show();

                    }
                }
        );

        requestQueue.add(getRequest);


        System.out.println("Ver recibos final ");

    }

    public  void datosReciboCableado(){
        ArrayList<Recibos> propietarioRecibos = null;
        propietarioRecibos = new ArrayList<>();

        propietarioRecibos.add(new Recibos(1,"Fecha 1 ", "Estado",2000, 1, 2 ,false ) )  ;
        propietarioRecibos.add(new Recibos(1,"Fecha 2 "," estado 2",3000, 4, 5 ,false)) ;
        propietarioRecibos.add(new Recibos(1,"Fecha 3 "," estado 3",4000, 7, 8  ,false )) ;
        propietarioRecibos.add(new Recibos(1,"Fecha 4 "," estado 4",5000, 10, 11 ,false )) ;
        propietarioRecibos.add(new Recibos(1,"Fecha 5 "," estado 5",6000, 1, 2  ,false)) ;



        adapter_recibos = new AdaptadorRecibos(propietarioRecibos);

        recycler_recibos.setAdapter(adapter_recibos);


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        System.out.println("Esto es lo que esta dentro el spinner " +adapterView.getItemAtPosition(i).toString());


        String numero = adapterView.getItemAtPosition(i).toString();

        final String cheque = "Cheque";
        final String deposito = "Deposito";
        final String transfe = "Transferencia";
        final String efectivo = "Efectivo";
        final String venezuela = "Venezuela";
        final String banesco = "Banesco";
        final String provincial = "Provincial";
        final String delTessoro = "Banco Del Tesoro";



        switch(numero) {
            case cheque:
                //Toast.makeText(getBaseContext(), "Single Click on position :"+adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();
                tipoCheqeEfectivo = cheque;
                break;
            case deposito:
                //Toast.makeText(getBaseContext(), "Single Click on position :"+adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();

                tipoCheqeEfectivo = deposito;
                break;
            case transfe:
                //Toast.makeText(getBaseContext(), "Single Click on position :"+adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();
                tipoCheqeEfectivo = transfe;

                break;

            case efectivo:
                //Toast.makeText(getBaseContext(), "Single Click on position :"+adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();
                tipoCheqeEfectivo = efectivo;

                break;


            case venezuela:
//                Toast.makeText(getBaseContext(), "Single Click on position : "+venezuela, Toast.LENGTH_SHORT).show();
                banco_emisor = venezuela;

                break;
            case banesco:
    //            Toast.makeText(getBaseContext(), "Single Click on position : "+banesco, Toast.LENGTH_SHORT).show();
                banco_emisor = banesco;

                break;
            case delTessoro:
  //              Toast.makeText(getBaseContext(), "Single Click on position : "+delTessoro, Toast.LENGTH_SHORT).show();
                banco_emisor = delTessoro;

                break;
            case provincial:
      //          Toast.makeText(getBaseContext(), "Single Click on position : "+provincial, Toast.LENGTH_SHORT).show();
                banco_emisor = provincial;

                break;


            default:
                banco_receptor_ = numero;
                System.out.println("Banco receptor "+numero);
                System.out.println("Banco receptor 2 "+banco_emisor);
                System.out.println("Banco receptor 3 "+tipoCheqeEfectivo);

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public static class  DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            // 1989-02-07

            // Create a new instance of DatePickerDialog and return it

            DatePickerDialog dayPickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_DARK, this, year, month, day);
            return dayPickerDialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            et_fecha.setText(tranformarFecha(month) + " " + day + " , " + year);
            month = month + 1;
            if (month < 10) {
                if (day > 10)
                    stringFechaParaRegistrar = "" + year + "-0" + month + "-" + day;
                else if (day < 10) {
                    stringFechaParaRegistrar = "" + year + "-0" + month + "-0" + day;

                }


            }
            if (month >= 10) {
                if (day > 10)
                    stringFechaParaRegistrar = "" + year + "-" + month + "-0" + day;
                else if (day < 10) {
                    stringFechaParaRegistrar = "" + year + "-" + month + "-" + day;

                }
            }
            // Create a new instance of DatePickerDialog and return it


        }
    }

        public static String tranformarFecha(int mes){
            String meses = "";


            switch (mes) {
                case 0:
                    meses ="Ene.";
                    break;
                case 1:
                    meses ="Feb.";
                    break;
                case 2:
                    meses ="Mar.";
                    break;
                case 3:
                    meses ="Abr.";
                    break;
                case 4:
                    meses ="May.";
                    break;
                case 5:
                    meses ="Jun.";
                    break;
                case 6:
                    meses ="Jul.";
                    break;
                case 7:
                    meses ="Ago.";
                    break;
                case 8:
                    meses ="Sep.";
                    break;
                case 9:
                    meses ="Oct.";
                    break;
                case 10:
                    meses ="Nov.";
                    break;
                case 11:
                    meses ="Dic.";
                    break;
            }

            return meses;
        }





}
