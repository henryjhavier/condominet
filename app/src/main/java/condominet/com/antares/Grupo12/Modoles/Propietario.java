package condominet.com.antares.Grupo12.Modoles;

/**
 * Created by h3nry on 21/6/2017.
 */

public class Propietario {
    int id , id_vivienda, id_junta;
    String cedula, nombre, apellido, fecha_de_nacimiento, telefono_local,
            celular1, celular2, correo, nombre_edificio, numero_vivienda;


    public Propietario(int id, int id_vivienda,int id_junta, String cedula, String nombre, String apellido, String fecha_de_nacimiento, String telefono_local, String celular1, String celular2, String correo, String nombre_edificio, String numero_vivienda) {
        this.id = id;
        this.id_vivienda = id_vivienda;
        this.id_junta = id_junta;
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fecha_de_nacimiento = fecha_de_nacimiento;
        this.telefono_local = telefono_local;
        this.celular1 = celular1;
        this.celular2 = celular2;
        this.correo = correo;
        this.nombre_edificio = nombre_edificio;
        this.numero_vivienda = numero_vivienda;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_vivienda() {
        return id_vivienda;
    }

    public void setId_vivienda(int id_vivienda) {
        this.id_vivienda = id_vivienda;
    }

    public int getId_junta() {
        return id_junta;
    }

    public void setId_junta(int id_junta) {
        this.id_junta = id_junta;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getFecha_de_nacimiento() {
        return fecha_de_nacimiento;
    }

    public void setFecha_de_nacimiento(String fecha_de_nacimiento) {
        this.fecha_de_nacimiento = fecha_de_nacimiento;
    }

    public String getTelefono_local() {
        return telefono_local;
    }

    public void setTelefono_local(String telefono_local) {
        this.telefono_local = telefono_local;
    }

    public String getCelular1() {
        return celular1;
    }

    public void setCelular1(String celular1) {
        this.celular1 = celular1;
    }

    public String getCelular2() {
        return celular2;
    }

    public void setCelular2(String celular2) {
        this.celular2 = celular2;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre_edificio() {
        return nombre_edificio;
    }

    public void setNombre_edificio(String nombre_edificio) {
        this.nombre_edificio = nombre_edificio;
    }

    public String getNumero_vivienda() {
        return numero_vivienda;
    }

    public void setNumero_vivienda(String numero_vivienda) {
        this.numero_vivienda = numero_vivienda;
    }
}
