package condominet.com.antares.Grupo12.Activades;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import condominet.com.antares.Grupo12.ConstantesURL;
import condominet.com.antares.R;

import static java.security.AccessController.getContext;

public class Configuracion extends AppCompatActivity {

    EditText ip_servidor;
    EditText id_inmueble;
    Button botonCambiar_ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);


            ip_servidor  = (EditText)  findViewById(R.id.ip_servidor);
            id_inmueble  = (EditText)  findViewById(R.id.id_inmueble);
            botonCambiar_ip  = (Button)  findViewById(R.id.btn_cambiar_ip);

            botonCambiar_ip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ConstantesURL.iP =        ip_servidor.getText().toString();
                    ConstantesURL.uRL_BASE = "http://"+ConstantesURL.iP +":3000/api";
                    ConstantesURL.id_inmueble = id_inmueble.getText().toString();
                    ConstantesURL.JsonUrlSolicitudServicios = ConstantesURL.uRL_BASE+"/v1/solicitud_proveedors/"+ConstantesURL.id_inmueble;

                    Toast.makeText(getApplicationContext(), "IP cambiada :"+ConstantesURL.uRL_BASE , Toast.LENGTH_SHORT).show();



                }

            });

        }


}
