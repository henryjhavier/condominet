package condominet.com.antares.Grupo09;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.nex3z.notificationbadge.NotificationBadge;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import condominet.com.antares.MainActivity;
import condominet.com.antares.Modelo.Propietario;
import condominet.com.antares.Modelo.Prueba;
import condominet.com.antares.Modelo.Recibo;
import condominet.com.antares.R;

/**
 * Created by Barbara on 22/06/2017.
 */

public class Perfil_propietario extends AppCompatActivity {

    TextView tvnombre, tvapelli, tvced, tvapto, tvfecha, tvlocal, tvcel1, tvcel2, tvcorreo, tvedif;
    NotificationBadge mBadge;
    private int mCount, sizeLista;

    private String TAG = Morosidad_edificio.class.getSimpleName();

    RequestQueue requestQueue;
    private ListView lvToolbarSearch;
    private  ReciboListAdapter adapter;
    ArrayList<Recibo> ListaRecibos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstancesState){
        super.onCreate(savedInstancesState);
        setContentView(R.layout.activity_perfil_propietario);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_PerfilPro);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });




        Intent intent = getIntent();
        final String idProp = intent.getStringExtra("id");
        String nombre= intent.getStringExtra("nombre");
        String apellido= intent.getStringExtra("apellido");
        final String cedula= intent.getStringExtra("cedula");
        String vivienda= intent.getStringExtra("vivienda");
        String telefono= intent.getStringExtra("telefono");
        String fecha_nacimiento= intent.getStringExtra("fecha_nacimiento");
        String celular1= intent.getStringExtra("celular1");
        String celular2= intent.getStringExtra("celular2");
        String correo= intent.getStringExtra("correo");


        tvnombre = (TextView) findViewById(R.id.tv_NombrePro);
        tvapelli = (TextView) findViewById(R.id.tv_ApelliPro);
        tvced = (TextView) findViewById(R.id.tv_CedulaPro);
        tvapto = (TextView) findViewById(R.id.tv_AptoProp);
        tvfecha = (TextView) findViewById(R.id.tv_FechNac);
        tvlocal = (TextView) findViewById(R.id.tv_TelfLocal);
        tvcel1 = (TextView) findViewById(R.id.tv_Cel1);
        tvcel2 = (TextView) findViewById(R.id.tv_Cel2);
        tvcorreo = (TextView) findViewById(R.id.tv_correoEl);


        tvnombre.setText(nombre);
        tvapelli.setText(apellido);
        tvced.setText(cedula);
        tvapto.setText(vivienda);
        tvfecha.setText(fecha_nacimiento);
        tvlocal.setText(telefono);
        tvcel1.setText(celular1);
        tvcel2.setText(celular2);
        tvcorreo.setText(correo);


        mCount= hacerGetRecibo(idProp);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
         @Override
         public void onClick(View view)
         {
             Intent intent = new Intent(Perfil_propietario.this, Recibos_pendientes.class);
             intent.putExtra("idProp",idProp);
             intent.putExtra("ListaRecibos",ListaRecibos);
             startActivity(intent);
          }
        });


    }

    public int hacerGetRecibo(String idProp) {

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, BaseURL.URL_BASE+"/recibos/"+idProp,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Gson gson = new GsonBuilder().disableHtmlEscaping().serializeNulls().create();
                        Prueba recibo = null;

                        try {
                            Log.d("mensaje","entre en el get11");
                            Type listType = new TypeToken<Prueba>(){}.getType();

                            recibo = gson.fromJson(response.toString(), listType);

                            ListaRecibos = recibo.getRecibos();

                            sizeLista= ListaRecibos.size();
                            // Crear un nuevo adaptador

                         /*   if (sizeLista==0)
                                android.animation.*/


                            mBadge = (NotificationBadge) findViewById(R.id.badge);
                            mBadge.setNumber(sizeLista);



                            adapter = new ReciboListAdapter(ListaRecibos,getApplicationContext());
                            lvToolbarSearch.setAdapter(adapter);



                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                        System.out.println("Resultados 4"+ recibo.getRecibos().size());

                        for (int i = 0; i < recibo.getRecibos().size(); i++) {

                            System.out.println("Resultados nombre "+recibo.getRecibos().get(i));

                        }

                    }





                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error.Response", "" + error);
                    }
                }

        );
// add it to the RequestQueue
        requestQueue.add(getRequest);

        return sizeLista;

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

