package condominet.com.antares.Grupo09;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import condominet.com.antares.Modelo.Evento;
import condominet.com.antares.Modelo.Morosidad;
import condominet.com.antares.R;

/**
 * Created by Daniel13 on 6/26/2017.
 */

public class NotificacionListAdapter extends ArrayAdapter<Evento> implements View.OnClickListener {


    ArrayList<Evento> dataSet;
    ArrayList<Evento> filterList;
    Context mContext;
    Filter filter;

    // View lookup cache
    private static class ViewHolder {
        TextView asunto;
        TextView mensaje;

    }

    public NotificacionListAdapter(ArrayList<Evento> data, Context context) {
        super(context, R.layout.celda_notificacion, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View view) {

    }

    private int lastPosition = -1;


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Evento evento = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        NotificacionListAdapter.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new NotificacionListAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.celda_notificacion, parent, false);
            viewHolder.asunto = (TextView) convertView.findViewById(R.id.contAsunto);
            viewHolder.mensaje = (TextView) convertView.findViewById(R.id.contMensaje);



            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (NotificacionListAdapter.ViewHolder) convertView.getTag();
            result=convertView;
        }

        // Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        //result.startAnimation(animation);
        //lastPosition = position;

        viewHolder.asunto.setText(evento.getAsunto());
        viewHolder.mensaje.setText(evento.getMensaje());

        // viewHolder.info.setOnClickListener(this);
        // viewHolder.info.setTag(position);
        // Return the completed view to render on screen


        return convertView;


    }

    @Override
    public Filter getFilter()
    {
        if(filter == null) {
            filter = new NotificacionListAdapter.CustomFilter();
        }
        return filter;
    }

    private class CustomFilter extends Filter
    {


        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // NOTE: this function is *always* called from a background thread, and
            // not the UI thread.
            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if(constraint != null && constraint.toString().length() > 0)
            {
                ArrayList<Evento> filt = new ArrayList<>();
                ArrayList<Evento> lItems = new ArrayList<>();
                synchronized (this)
                {
                    lItems.addAll(dataSet);
                }
                for(int i = 0, l = lItems.size(); i < l; i++)
                {
                    Evento evento = lItems.get(i);
                    if
                            (evento.getAsunto().toLowerCase().contains(constraint) || evento.getMensaje().contains(constraint) )
                        filt.add(evento);
                }

                result.count = filt.size();
                result.values = filt;
            }
            else
            {


                synchronized(this)
                {

                    result.values = dataSet;
                    result.count = dataSet.size();

                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            // NOTE: this function is *always* called from the UI thread.

            filterList = (ArrayList<Evento>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0, l = filterList.size(); i < l; i++)
                add(filterList.get(i));
            notifyDataSetInvalidated();



        }

    }





}
