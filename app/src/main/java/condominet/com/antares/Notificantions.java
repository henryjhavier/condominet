package condominet.com.antares;


import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import condominet.com.antares.Grupo12.Adaptadores.AdaptadorRecibos;
import condominet.com.antares.Grupo12.Adaptadores.AdaptadorSolicitudServicio;
import condominet.com.antares.Grupo12.ConstantesURL;
import condominet.com.antares.Grupo12.Modoles.Recibos;
import condominet.com.antares.Grupo12.Modoles.Solicitud_servicios;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.NOTIFICATION_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class Notificantions extends Fragment {


    public Notificantions() {
        // Required empty public constructor
    }

    RequestQueue requestQueue;
    private AdaptadorSolicitudServicio adapter_servicio;
    RecyclerView rv_solicitud_servicio;

    private RecyclerView.LayoutManager lManager_sServicios;
    private SwipeRefreshLayout refreshLayout;
    RelativeLayout relativeLayout_sin_proveedores ;
    Gson gsonSharedPreference;
    String json;
    SharedPreferences.Editor editor;
    SharedPreferences sp;

    String urlSolicitudProveedor;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewNotificaciones = inflater.inflate(R.layout.fragment_notificantions, container, false);


        rv_solicitud_servicio = (RecyclerView) viewNotificaciones.findViewById(R.id.recicladorNotificaciones);
        refreshLayout = (SwipeRefreshLayout) viewNotificaciones.findViewById(R.id.swipeRefresh);
        relativeLayout_sin_proveedores = (RelativeLayout) viewNotificaciones.findViewById(R.id.rl_no_hay_resultados_busqueda_servicios);

        urlSolicitudProveedor = ConstantesURL.JsonUrlSolicitudServicios ;
        System.out.println("URL solcitud "+urlSolicitudProveedor);



        rv_solicitud_servicio.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager_sServicios = new LinearLayoutManager(getContext());
        rv_solicitud_servicio.setLayoutManager(lManager_sServicios);

        try {
            requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

       //requestSolicitud();
        //   notification1();



        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {

                        System.out.println("Refrescar ");

                        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, ConstantesURL.JsonUrlSolicitudServicios,
                                new Response.Listener<JSONArray>() {
                                    @Override
                                    public void onResponse(JSONArray response) {

                                        System.out.println(" Response "+response);
                                        Gson gson = new GsonBuilder().disableHtmlEscaping().serializeNulls().create();
                                        ArrayList<Solicitud_servicios> propietarioSolicitudes = null;

                                        try {
                                            Type listType = new TypeToken<ArrayList<Solicitud_servicios>>(){}.getType();

                                            propietarioSolicitudes = gson.fromJson(response.toString(), listType);


                                            for (int i = 0; i < propietarioSolicitudes.size(); i++) {

                                                System.out.println("Resultados recibos tipoServicio "+propietarioSolicitudes.get(i).getTipo_servicio());

                                                if ( i == propietarioSolicitudes.size() -1 ){
                                                    notification1(propietarioSolicitudes.get(i).getTipo_servicio(), propietarioSolicitudes.get(i).getId(),
                                                            propietarioSolicitudes.get(i).getNombre_propietario() +" "+propietarioSolicitudes.get(i).getApellido()
                                                    );
                                                }
                                            }

                                            adapter_servicio = new AdaptadorSolicitudServicio(propietarioSolicitudes);

                                            rv_solicitud_servicio.setAdapter(adapter_servicio);


                            if (propietarioSolicitudes.size() == 0 ){

                                relativeLayout_sin_proveedores.setVisibility(View.VISIBLE);
                                rv_solicitud_servicio.setVisibility(View.GONE);
                            }
                            else{
                                relativeLayout_sin_proveedores.setVisibility(View.GONE);
                                rv_solicitud_servicio.setVisibility(View.VISIBLE);

                            }

                                        } catch (JsonSyntaxException e) {
                                            e.printStackTrace();
                                        }

                                        refreshLayout.setRefreshing(false);


                                    }



                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e("Error.Response", "" + error);
                                        Toast.makeText(getContext(), "Error.Response recibos :"+error, Toast.LENGTH_SHORT).show();
                                        refreshLayout.setRefreshing(false);
                                    }
                                }
                        );

                        requestQueue.add(getRequest);


                    }
                });


        return viewNotificaciones;

    }


    public void notification1(String titulo , int id, String contenido) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getContext())
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(titulo)
                        .setContentText(contenido);

        Intent resultIntent = new Intent(getContext(), Login.class);

// Because clicking the notification opens a new ("special") activity, there's
// no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        getContext(),
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);

        // Sets an ID for the notification
        //int mNotificationId = 001;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getContext().getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(id, mBuilder.build());

    }


}
