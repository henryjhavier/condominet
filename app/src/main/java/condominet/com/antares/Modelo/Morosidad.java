package condominet.com.antares.Modelo;

/**
 * Created by Daniel13 on 6/24/2017.
 */

public class Morosidad {

    String cedula;
    String nombre;
    String apellido;
    String telefono;
    String correo;
    String edificio;
    String vivienda;

    String meses;

    String cantidad;


    public Morosidad(String cedula, String nombre, String apellido, String telefono, String correo, String edificio, String vivienda, String meses, String cantidad) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.correo = correo;
        this.edificio = edificio;
        this.vivienda = vivienda;
        this.meses = meses;
        this.cantidad = cantidad;
    }


    public Morosidad(String cedula, String nombre, String apellido, String vivienda) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.vivienda = vivienda;
    }


    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    public String getVivienda() {
        return vivienda;
    }

    public void setVivienda(String vivienda) {
        this.vivienda = vivienda;
    }

    public String getMeses() {
        return meses;
    }

    public void setMeses(String meses) {
        this.meses = meses;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }
}
