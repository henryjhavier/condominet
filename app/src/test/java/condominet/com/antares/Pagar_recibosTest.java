package condominet.com.antares;

import org.junit.Test;
import junit.framework.Assert;

import java.util.ArrayList;

import condominet.com.antares.Grupo12.Modoles.Recibos;

import static org.junit.Assert.*;

/**
 * Created by h3nry on 3/7/2017.
 */
public class Pagar_recibosTest {

    ArrayList<Recibos> propietarioRecibos;

    public Pagar_recibosTest() {
        propietarioRecibos = null;
        propietarioRecibos = new ArrayList<>();

        propietarioRecibos.add(new Recibos(1,"Fecha 1 ", "NO PAGADO", 2000, 1, 2  ,false)) ;
        propietarioRecibos.add(new Recibos(1,"Fecha 2 ", "NO PAGADO", 2000, 1, 3  ,false)) ;
        propietarioRecibos.add(new Recibos(1,"Fecha 2 ", "NO PAGADO", 2000, 1, 3  ,false)) ;
        propietarioRecibos.add(new Recibos(1,"Fecha 2 ", "NO PAGADO", 2000, 1, 3  ,true)) ;
    }


    @Test
    public void testVerRecibosPendientes1() {


        Assert.assertTrue("El propietario tiene recibos ", propietarioRecibos.size() != 0 );
    }

    @Test
    public void testVerRecibosPendientes2() {
        ArrayList<Recibos> propietarioRecibos = null;
        propietarioRecibos = new ArrayList<>();

        //assertEquals ("El propietario no tiene recibos pendientes", propietarioRecibos.size(), 0);
        Assert.assertEquals("El propietario no tiene recibos pendientes", propietarioRecibos.size(), 0);
    }

    @Test
    public void sumarMonto() throws Exception {

            assertEquals(4000, propietarioRecibos.get(0).getMonto() + propietarioRecibos.get(1).getMonto());

    }






}