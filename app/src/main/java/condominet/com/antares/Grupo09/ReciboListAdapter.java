package condominet.com.antares.Grupo09;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import condominet.com.antares.Modelo.Recibo;
import condominet.com.antares.R;

/**
 * Created by Barbara on 26/06/2017.
 */

public class ReciboListAdapter extends ArrayAdapter<Recibo> implements View.OnClickListener
{

   ArrayList<Recibo> ListaRecibos;
    Context mContext;

    public static class ViewHolder
    {

        TextView itemDetalles;
        TextView itemFechas;
        TextView itemMontos;
    }

    public ReciboListAdapter(ArrayList<Recibo> data, Context context) {
        super(context, R.layout.celda_recibo, data);
        this.ListaRecibos = data;
        this.mContext=context;

    }


/* public ViewHolder(View itemView)
 {
     super(itemView);
     itemDetalles = (TextView) itemView.findViewById(R.id.item_detalle);
     itemFechas = (TextView) itemView.findViewById(R.id.item_fecha);
     itemMontos = (TextView) itemView.findViewById(R.id.item_monto);

 }*/

    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Recibo recibo = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ReciboListAdapter.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.celda_recibo, parent, false);

            viewHolder.itemDetalles = (TextView) convertView.findViewById(R.id.item_detalle);
            viewHolder.itemFechas = (TextView) convertView.findViewById(R.id.item_fecha);
            viewHolder.itemMontos = (TextView) convertView.findViewById(R.id.item_monto);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ReciboListAdapter.ViewHolder) convertView.getTag();
            result=convertView;
        }

        viewHolder.itemDetalles.setText(recibo.getEstado());
        viewHolder.itemFechas.setText(recibo.getFecha());
        viewHolder.itemMontos.setText(recibo.getMonto());

        return convertView;
    }

    @Override
    public void onClick(View view) {

    }



}
