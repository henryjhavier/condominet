package condominet.com.antares.Grupo09;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.nex3z.notificationbadge.NotificationBadge;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import condominet.com.antares.R;

/**
 * Created by Aqurdaneta on 27-06-2017.
 */

public class Evento_detalle extends AppCompatActivity {

    TextView tvasunto,tvmensaje,tvprioridad,tvfechaOcurrencia,tvhoraOcurrencia;
    final Context context = this;
    final String ID_JUNTA = "1";
    final String JsonURLParaNotificacion = BaseURL.URL_BASE+"/alerts/";

    @Override
    protected void onCreate(Bundle savedInstancesState){
        super.onCreate(savedInstancesState);
        setContentView(R.layout.activity_evento_detalle);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_DetalleEven);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        final String id = intent.getStringExtra("id");
        String asunto= intent.getStringExtra("asunto");
        String mensaje= intent.getStringExtra("mensaje");
        String prioridad= intent.getStringExtra("prioridad");
        String fecha= intent.getStringExtra("fechaOcurrencia");
        String hora= intent.getStringExtra("horaOcurrencia");

        tvasunto = (TextView) findViewById(R.id.tv_AsuntoEven);
        tvmensaje = (TextView) findViewById(R.id.tv_MensajeEven);
        tvprioridad = (TextView) findViewById(R.id.tv_PrioridadEven);
        tvfechaOcurrencia = (TextView) findViewById(R.id.tv_FechaEven);
        tvhoraOcurrencia = (TextView) findViewById(R.id.tv_HoraEven);

        tvasunto.setText(asunto);
        tvmensaje.setText(mensaje);
        tvprioridad.setText(prioridad);
        tvfechaOcurrencia.setText(fecha);
        tvhoraOcurrencia.setText(hora);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                try {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    eliminarEvento(id);
                    alert(alertDialogBuilder);
                    Intent intent = new Intent(Evento_detalle.this, Notificaciones_eventualidad.class);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void eliminarEvento(String id){
        RequestQueue requestQueue;

        requestQueue = Volley.newRequestQueue(this);
        Map<String, String> jsonParams = new HashMap<String, String>();

        jsonParams.put("id",id.toString());

        JsonObjectRequest postRequest = new JsonObjectRequest( Request.Method.DELETE, JsonURLParaNotificacion+id.toString(),

                new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Handle Error
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };
        requestQueue.add(postRequest);
    }

    public void alert(AlertDialog.Builder alertDialogBuilder) throws Exception {
        alertDialogBuilder.setTitle("Evento Eliminado.");
        alertDialogBuilder
                .setMessage("El evento fue eliminado exitosamente.")
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
