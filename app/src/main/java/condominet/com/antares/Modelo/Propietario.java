package condominet.com.antares.Modelo;


import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Propietario {

    String id;
    String nombre;
    String apellido;
    String cedula;
    String vivienda;

    @Nullable
    @SerializedName("fecha_nacimiento")
    String fecha_nacimiento;

    @Nullable
    @SerializedName("telefono_local")
    String telefono;

    @Nullable
    @SerializedName("celular1")
    String celular1;

    @Nullable
    @SerializedName("celular2")
    String celular2;

    String correo;

   /* @Nullable
    @SerializedName("edificio")
    String edificio;*/

    @Nullable
    @SerializedName("deuda")
    private
    long deuda;

    @Nullable
    @SerializedName("num")
    private
    int meses;




    public Propietario(String cedula) {
    }

    public Propietario(String id, String cedula, String nombre, String apellido, String fecha_nacimiento,
                       String telefono, String celular1, String celular2, String correo,
                        String vivienda, long deuda, int meses) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.fecha_nacimiento = fecha_nacimiento;
        this.telefono = telefono;
        this.celular1 = celular1;
        this.celular2= celular2;
        this.correo = correo;
       // this.edificio = edificio;
        this.vivienda = vivienda;
        this.deuda = deuda;
        this.meses = meses;

    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getVivienda() {
        return vivienda;
    }

    public void setVivienda(String vivienda) {
        this.vivienda = vivienda;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono= telefono;
    }

    public String getCelular1() {
        return celular1;
    }

    public void setCelular1(String celular1) {
        this.celular1 = celular1;
    }

    public String getCelular2() {
        return celular2;
    }

    public void setCelular2(String celular2) {
        this.celular2 = celular2;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

  /*  public String getEdificio() { return edificio; }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }*/

    public String getFecha_de_nacimiento() { return fecha_nacimiento;}

    public void setFecha_de_nacimiento(String fecha_nacimiento) { this.fecha_nacimiento = fecha_nacimiento;}


    @Nullable
    public long getDeuda() {
        return deuda;
    }

    public void setDeuda(@Nullable long deuda) {
        this.deuda = deuda;
    }

    @Nullable
    public int getMeses() {
        return meses;
    }

    public void setMeses(@Nullable int meses) {
        this.meses = meses;
    }
}
