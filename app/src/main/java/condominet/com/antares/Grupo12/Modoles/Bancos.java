package condominet.com.antares.Grupo12.Modoles;

/**
 * Created by h3nry on 3/7/2017.
 */

public class Bancos {
    int id, administradora_junta_condominios_id;
    String banco , numero_cuenta, benecifiario;

    public Bancos(int id, int administradora_junta_condominios_id, String banco, String numero_cuenta, String benecifiario) {
        this.id = id;
        this.administradora_junta_condominios_id = administradora_junta_condominios_id;
        this.banco = banco;
        this.numero_cuenta = numero_cuenta;
        this.benecifiario = benecifiario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAdministradora_junta_condominios_id() {
        return administradora_junta_condominios_id;
    }

    public void setAdministradora_junta_condominios_id(int administradora_junta_condominios_id) {
        this.administradora_junta_condominios_id = administradora_junta_condominios_id;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getNumero_cuenta() {
        return numero_cuenta;
    }

    public void setNumero_cuenta(String numero_cuenta) {
        this.numero_cuenta = numero_cuenta;
    }

    public String getBenecifiario() {
        return benecifiario;
    }

    public void setBenecifiario(String benecifiario) {
        this.benecifiario = benecifiario;
    }
}
