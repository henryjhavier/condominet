package condominet.com.antares.Grupo12.Modoles;

/**
 * Created by h3nry on 21/6/2017.
 */

public class Recibos {
    int id;
    String fecha, estado;
    int monto , administradora_vivienda_id, administradora_inmueble_id ;
    Boolean seleccionado;


    public Recibos(int id, String fecha, String estado, int monto, int administradora_vivienda_id, int administradora_inmueble_id, Boolean seleccionado) {
        this.id = id;
        this.fecha = fecha;
        this.estado = estado;
        this.monto = monto;
        this.administradora_vivienda_id = administradora_vivienda_id;
        this.administradora_inmueble_id = administradora_inmueble_id;
        this.seleccionado = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }


    public Boolean getSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(Boolean seleccionado) {
        this.seleccionado = seleccionado;
    }
}
