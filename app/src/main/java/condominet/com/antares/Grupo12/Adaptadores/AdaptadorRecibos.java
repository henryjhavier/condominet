package condominet.com.antares.Grupo12.Adaptadores;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import condominet.com.antares.Grupo12.Modoles.Recibos;
import condominet.com.antares.R;

/**
 * Created by h3nry on 21/6/2017.
 */


public class AdaptadorRecibos extends RecyclerView.Adapter<AdaptadorRecibos.RecibosViewHolder> {
    private ArrayList<Recibos> items;

    public static class RecibosViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView fecha;
        public TextView monto;
        public ImageView seleccionado ;

        public RecibosViewHolder(View v) {
            super(v);
            fecha = (TextView) v.findViewById(R.id.tv_fecha_recibo);
            monto = (TextView) v.findViewById(R.id.tv_monto_recibo);
            seleccionado = (ImageView) v.findViewById(R.id.iv_seleccionado);

            this.setIsRecyclable (false);

        }
    }

    public AdaptadorRecibos(ArrayList<Recibos> items) {
        this.items = items;
    }

    public int getMontoMes(int position) {

        int monto = 0 ;

        try {

            System.out.println("Esta seleccionado o no "+items.get(position).getSeleccionado());
            if ( items.get(position).getSeleccionado())
            {
                items.get(position).setSeleccionado(false);
                monto = monto - items.get(position).getMonto();
            }
            else
            {
                items.get(position).setSeleccionado(true);
                monto  = items.get(position).getMonto();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        notifyDataSetChanged();
        return monto ;
    }


    public int getIdRecibo(int position) {

        int monto = 0 ;

        try {
            monto  = items.get(position).getId();
        } catch (Exception e) {
            e.printStackTrace();
        }


        notifyDataSetChanged();
        return monto ;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public RecibosViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.esqueleto_recibos, viewGroup, false);
        return new RecibosViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecibosViewHolder viewHolder, int i) {


        System.out.println(" Seleccion"+items.get(i).getSeleccionado());

        if (items.get(i).getSeleccionado()){

            viewHolder.seleccionado.setImageResource(R.mipmap.ic_check);
        }

        if (!items.get(i).getSeleccionado()){

            viewHolder.seleccionado.setImageResource(R.mipmap.ic_unchecked);
        }


        try {
            viewHolder.fecha.setText( items.get(i).getFecha());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            String monto = items.get(i).getMonto()+"";
            viewHolder.monto.setText("Monto a pagar "+monto+" Bs.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}