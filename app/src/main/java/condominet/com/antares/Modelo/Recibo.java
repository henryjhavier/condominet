package condominet.com.antares.Modelo;


import java.io.Serializable;

/**
 * Created by Barbara on 28/06/2017.
 */

public class Recibo implements Serializable {
    int idProp;
    String fecha;
    String estado;
    String monto;
    int apartamento_id;
    int propietario_id;

    public Recibo(int idProp, String fecha, String estado, String monto, int apartamento_id, int propietario_id) {

        this.idProp = idProp;
        this.fecha = fecha;
        this.estado = estado;
        this.monto = monto;
        this.apartamento_id = apartamento_id;
        this.propietario_id = propietario_id;
    }

    public Recibo() {

    }

    public int getIdProp() {
        return idProp;
    }

    public void setIdProp(int idProp) {
        this.idProp = idProp;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public int getApartamento_id() {
        return apartamento_id;
    }

    public void setApartamento_id(int apartamento_id) {
        this.apartamento_id = apartamento_id;
    }

    public int getPropietario_id() {
        return propietario_id;
    }

    public void setPropietario_id(int propietario_id) {
        this.propietario_id = propietario_id;
    }

}