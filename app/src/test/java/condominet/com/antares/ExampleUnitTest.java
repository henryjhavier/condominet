package condominet.com.antares;

import org.junit.Test;

import java.util.ArrayList;

import condominet.com.antares.Grupo09.Consultar_propietario;
import condominet.com.antares.Grupo09.Morosidad_edificio;
import condominet.com.antares.Grupo09.Notificaciones_eventualidad;
import condominet.com.antares.Modelo.Evento;
import condominet.com.antares.Modelo.Morosidad;
import condominet.com.antares.Modelo.Propietario;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    ArrayList<Propietario> propietarioFilt = new ArrayList<>();
    ArrayList<Morosidad> morosidadFilt = new ArrayList<>();
    ArrayList<Evento> notificacionFilt = new ArrayList<>();

    @Test
    public void verificarDatosIncompletosConsultaPro() throws  Exception {


       int hayDatos =  propietarioFilt.size();

        String mensajeError;
        Consultar_propietario propietarios = new Consultar_propietario();
        mensajeError = propietarios.validarIncompletoPro(hayDatos);

        assertEquals(mensajeError,"No existe usuario con el(los) dato(s) ingresados");
    }


    @Test
    public void verificarDatosIncompletosMorosidadPro() throws  Exception {


        int hayDatos =  morosidadFilt.size();
        String mensajeError;
        Morosidad_edificio morosos = new Morosidad_edificio();
        mensajeError = morosos.validarIncompletoMo(hayDatos);

        assertEquals(mensajeError,"No existe usuario con el(los) dato(s) ingresados");
    }

    @Test
    public void verificarDatosIncompletosNotificaciones() throws  Exception {


        int hayDatos =  notificacionFilt.size();
        String mensajeError;
        Notificaciones_eventualidad notificaciones = new Notificaciones_eventualidad();
        mensajeError = notificaciones.validarIncompletoNoti(hayDatos);

        assertEquals(mensajeError,"No existen notificaciones con el(los) dato(s) ingresados");
    }


}