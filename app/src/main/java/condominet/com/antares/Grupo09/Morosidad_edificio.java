package condominet.com.antares.Grupo09;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import condominet.com.antares.MainActivity;
import condominet.com.antares.Modelo.Morosidad;
import condominet.com.antares.Modelo.Propietario;
import condominet.com.antares.Modelo.Prueba;
import condominet.com.antares.R;

public class Morosidad_edificio extends AppCompatActivity implements SearchView.OnQueryTextListener{

        private Toolbar tbMainSearch;
        private ListView lvToolbarSearch;
        final String ID_JUNTA = "1";
        private String TAG = Morosidad_edificio.class.getSimpleName();
        final String JsonURLParaLosMorosos = BaseURL.URL_BASE+"/morosidad/"+ID_JUNTA;
        ArrayList<Propietario> listaPropietarios;
        private  PropietarioListAdapter adapter;
        RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_morosidad_edificio);
        setUpViews();

        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbarSearch);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        final SwipeRefreshLayout mySwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefreshMo);


        lvToolbarSearch.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                Propietario selectedProduct = listaPropietarios.get(i);

                Intent intent = new Intent(getApplicationContext(),Detalle_morosidad.class);
                intent.putExtra("nombre",selectedProduct.getNombre());
                intent.putExtra("apellido",selectedProduct.getApellido());
                intent.putExtra("cedula",selectedProduct.getCedula());
                intent.putExtra("telefono",selectedProduct.getTelefono());
                intent.putExtra("correo",selectedProduct.getCorreo());
              //  intent.putExtra("edificio",selectedProduct.getEdificio());
                intent.putExtra("vivienda",selectedProduct.getVivienda());
                intent.putExtra("meses",String.valueOf(selectedProduct.getMeses()));
                intent.putExtra("deuda",String.valueOf(selectedProduct.getDeuda()));


                startActivity(intent);
            }
        });


        hacerGetMorosos();

        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {


                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.
                        hacerGetMorosos();
                        mySwipeRefreshLayout.setRefreshing(false);
                    }
                }
        );



    }

    private void hacerGetMorosos() {

        requestQueue = Volley.newRequestQueue(getApplicationContext());


        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, JsonURLParaLosMorosos,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Gson gson = new GsonBuilder().disableHtmlEscaping().serializeNulls().create();
                        Prueba morosos = null;

                        try {
                            Log.d("mensaje","entre en el get11");
                            Type listType = new TypeToken<Prueba>(){}.getType();

                            morosos = gson.fromJson(response.toString(), listType);

                            listaPropietarios = morosos.getPropietarios();

                          //  Log.d("size", String.valueOf(listaMorosidades.size()));

                            // Crear un nuevo adaptador


                            adapter = new PropietarioListAdapter(listaPropietarios,getApplicationContext());
                            lvToolbarSearch.setAdapter(adapter);



                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Resultados 4"+ morosos.getPropietarios().size());

                        for (int i = 0; i < morosos.getPropietarios().size(); i++) {

                            System.out.println("Resultados nombre "+morosos.getPropietarios().get(i));

                        }

                    }





                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error.Response", "" + error);
                    }
                }
        );
// add it to the RequestQueue
        requestQueue.add(getRequest);

    }


    private void setUpViews() {
        tbMainSearch = (Toolbar)findViewById(R.id.tb_toolbarSearch);
        lvToolbarSearch =(ListView) findViewById(R.id.lv_toolbarSearch);
       // listaMorosidades = new ArrayList<>();

        hacerGetMorosos();

        setSupportActionBar(tbMainSearch);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_propietario, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem mSearchmenuItem = menu.findItem(R.id.menu_toolbarsearch);
        SearchView searchView = (SearchView) mSearchmenuItem.getActionView();
        searchView.setQueryHint("Ingrese Nombre, Cédula o Número de Apartamento");
        searchView.setOnQueryTextListener(this);
        Log.d(TAG, "onCreateOptionsMenu: mSearchmenuItem->" + mSearchmenuItem.getActionView());
        return true;
    }
    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d(TAG, "onQueryTextSubmit: query->"+query);
        return true;
    }



    @Override
    public boolean onQueryTextChange(String newText) {

        Log.d("mensaje", newText);

        if (newText.isEmpty())
        {

            setUpViews();


        }else {

            try {

                Log.d(TAG, "onQueryTextChange: newText->" + newText);
                adapter.getFilter().filter(newText);

                int hayDatos = adapter.getCount();
                validarIncompletoMo(hayDatos);
                mostrarMensajeEror();

            } catch (Exception e)
            {
                String mensajeError = "No hay morosos regisrados";
                Toast.makeText(getApplicationContext(), mensajeError, Toast.LENGTH_SHORT).show();
            }
        }



        return true;
    }

    private void mostrarMensajeEror() {

        String mensajeError = "No existe usuario con el(los) dato(s) ingresados";

        Toast.makeText(getApplicationContext(), mensajeError, Toast.LENGTH_SHORT).show();
    }

    public String validarIncompletoMo(int parametroEnt){

        String mensajeError = "";

        if ( parametroEnt == 0){

            mensajeError = "No existe usuario con el(los) dato(s) ingresados";


        }

        return mensajeError;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_home:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
