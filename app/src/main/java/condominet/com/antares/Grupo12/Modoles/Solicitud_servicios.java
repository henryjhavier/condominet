package condominet.com.antares.Grupo12.Modoles;

/**
 * Created by h3nry on 2/7/2017.
 */

public class Solicitud_servicios {
    int id;
    String nombre, apellido;
    int apartamento;
    String tipo_servicio, correo, fecha, estatus;
    int administradora_owners_id, id_inmueble, id_vivienda;
    String numero_vivienda, nombre_propietario, apellido_propietario ;
    int id_propietario;
    String cedula_propietario;

    public Solicitud_servicios(int id, String nombre, String apellido, int apartamento, String tipo_servicio, String correo, String fecha, String estatus, int administradora_owners_id, int id_inmueble, int id_vivienda, String numero_vivienda, String nombre_propietario, String apellido_propietario, int id_propietario, String cedula_propietario) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.apartamento = apartamento;
        this.tipo_servicio = tipo_servicio;
        this.correo = correo;
        this.fecha = fecha;
        this.estatus = estatus;
        this.administradora_owners_id = administradora_owners_id;
        this.id_inmueble = id_inmueble;
        this.id_vivienda = id_vivienda;
        this.numero_vivienda = numero_vivienda;
        this.nombre_propietario = nombre_propietario;
        this.apellido_propietario = apellido_propietario;
        this.id_propietario = id_propietario;
        this.cedula_propietario = cedula_propietario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getApartamento() {
        return apartamento;
    }

    public void setApartamento(int apartamento) {
        this.apartamento = apartamento;
    }

    public String getTipo_servicio() {
        return tipo_servicio;
    }

    public void setTipo_servicio(String tipo_servicio) {
        this.tipo_servicio = tipo_servicio;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public int getAdministradora_owners_id() {
        return administradora_owners_id;
    }

    public void setAdministradora_owners_id(int administradora_owners_id) {
        this.administradora_owners_id = administradora_owners_id;
    }

    public int getId_inmueble() {
        return id_inmueble;
    }

    public void setId_inmueble(int id_inmueble) {
        this.id_inmueble = id_inmueble;
    }

    public int getId_vivienda() {
        return id_vivienda;
    }

    public void setId_vivienda(int id_vivienda) {
        this.id_vivienda = id_vivienda;
    }

    public String getNumero_vivienda() {
        return numero_vivienda;
    }

    public void setNumero_vivienda(String numero_vivienda) {
        this.numero_vivienda = numero_vivienda;
    }

    public String getNombre_propietario() {
        return nombre_propietario;
    }

    public void setNombre_propietario(String nombre_propietario) {
        this.nombre_propietario = nombre_propietario;
    }

    public int getId_propietario() {
        return id_propietario;
    }

    public void setId_propietario(int id_propietario) {
        this.id_propietario = id_propietario;
    }

    public String getCedula_propietario() {
        return cedula_propietario;
    }

    public void setCedula_propietario(String cedula_propietario) {
        this.cedula_propietario = cedula_propietario;
    }

    public String getApellido_propietario() {
        return apellido_propietario;
    }

    public void setApellido_propietario(String apellido_propietario) {
        this.apellido_propietario = apellido_propietario;
    }
}




