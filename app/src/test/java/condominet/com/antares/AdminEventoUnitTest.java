package condominet.com.antares;

import static junit.framework.Assert.assertEquals;
import org.junit.Test;
import condominet.com.antares.Grupo09.Admin_eventualidad;
import condominet.com.antares.Grupo09.Evento_detalle;
import condominet.com.antares.Modelo.mensajeObj;

/**
 * Created by Aqurdaneta on 28-06-2017.
 */

public class AdminEventoUnitTest {

    @Test
    public void verificarCrearEventoSinAsunto() throws  Exception {

        String asuntoVacio = "";
        String mensaje = "Mesaje de prueba.";
        int prioridad = 1;
        Admin_eventualidad AdministradorEven = new Admin_eventualidad();
        mensajeObj mensajeError = AdministradorEven.crearEvento(asuntoVacio,mensaje,prioridad);

        assertEquals(mensajeError.getTitulo(),"Error!");
        assertEquals(mensajeError.getMensaje(),"El campo Asunto se encuentra vacio. Por favor insertar valor.");
        assertEquals(mensajeError.getTipo(),0);
    }

    @Test
    public void verificarCrearEventoSinMensaje() throws  Exception {

        String asuntoVacio = "Titulo de Prueba.";
        String mensaje = "";
        int prioridad = 1;
        Admin_eventualidad AdministradorEven = new Admin_eventualidad();
        mensajeObj mensajeError = AdministradorEven.crearEvento(asuntoVacio,mensaje,prioridad);

        assertEquals(mensajeError.getTitulo(),"Error!");
        assertEquals(mensajeError.getMensaje(),"El campo Mensaje se encuentra vacio. Por favor insertar valor.");
        assertEquals(mensajeError.getTipo(),0);
    }

    @Test
    public void verificarCrearEventoSinPrioridadAsignada() throws  Exception {

        String asuntoVacio = "Titulo de Prueba.";
        String mensaje = "Mesaje de prueba.";
        int prioridad = 0;
        Admin_eventualidad AdministradorEven = new Admin_eventualidad();
        mensajeObj mensajeError = AdministradorEven.crearEvento(asuntoVacio,mensaje,prioridad);

        assertEquals(mensajeError.getTitulo(),"Error!");
        assertEquals(mensajeError.getMensaje(),"No ha sido elegido el tipo de prioridad.");
        assertEquals(mensajeError.getTipo(),0);
    }

    @Test
    public void verificarCrearEventoConExito() throws  Exception {

        String asuntoVacio = "Titulo de Prueba.";
        String mensaje = "Mesaje de prueba.";
        int prioridad = 1;
        Admin_eventualidad AdministradorEven = new Admin_eventualidad();
        mensajeObj mensajeError = AdministradorEven.crearEvento(asuntoVacio,mensaje,prioridad);

        assertEquals(mensajeError.getTitulo(),"Evento Creado!");
        assertEquals(mensajeError.getMensaje(),"El evento fue creado exitosamente.");
        assertEquals(mensajeError.getTipo(),1);
    }

    @Test
    public void verificarCreacionDeEventoExitosa() throws  Exception {

        String asuntoVacio = "Titulo de Prueba.";
        String mensaje = "Mesaje de prueba.";
        String prioridad = "Urgente";
        Admin_eventualidad AdministradorEven = new Admin_eventualidad();
        String mensajeError = AdministradorEven.postJSON(asuntoVacio,mensaje,prioridad);

        assertEquals(mensajeError,"Creacion Exitosa.");
    }

    @Test
    public void verificarCreacionDeEventoFallida() throws  Exception {

        String asuntoVacio = "Titulo de Prueba.";
        String mensaje = "Mesaje de prueba.";
        String prioridad = "Urgente";
        Admin_eventualidad AdministradorEven = new Admin_eventualidad();
        String mensajeError = AdministradorEven.postJSON(asuntoVacio,mensaje,prioridad);

        assertEquals(mensajeError,"Creacion Fallida.");
    }

}
