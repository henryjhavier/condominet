package condominet.com.antares.Grupo12.Adaptadores;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;

import java.util.ArrayList;

import condominet.com.antares.Grupo12.Modoles.Propietario;
import condominet.com.antares.R;

/**
 * Created by h3nry on 21/6/2017.
 */


public class AdaptadorPropietarios extends RecyclerView.Adapter<AdaptadorPropietarios.PropietarioViewHolder> {
    private ArrayList<Propietario> items;
    private ArrayList<Propietario> itemsRespaldo;

    public static class PropietarioViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public CircularImageView imagen;
        public TextView nombre;
        public TextView Apartamento;
        public TextView cedula;

        public PropietarioViewHolder(View v) {
            super(v);
            imagen = (CircularImageView) v.findViewById(R.id.imageView2);
            nombre = (TextView) v.findViewById(R.id.tv_propietario_nombre);
            Apartamento = (TextView) v.findViewById(R.id.tv_propietario_apellido);
            cedula = (TextView) v.findViewById(R.id.tv_propietario_cedula);
        }
    }

    public AdaptadorPropietarios(ArrayList<Propietario> items) {
        this.items = items;
        this.itemsRespaldo = items;

    }


    public int getItemCountRespaldo() {
        return itemsRespaldo.size();
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public int getPropietarioId(int position) {
        int idPropietario;

        try {
            idPropietario = items.get(position).getId();
        } catch (Exception e) {
            idPropietario = 0 ;
            e.printStackTrace();
        }
        return  idPropietario;
    }
    public int getPropietarioIdVivienda(int position) {
        int idPropietario;

        try {
            idPropietario = items.get(position).getId_vivienda();
        } catch (Exception e) {
            idPropietario = 0 ;
            e.printStackTrace();
        }
        return  idPropietario;
    }
    public int getPropietarioIdJunta(int position) {
        int idPropietario;

        try {
            idPropietario = items.get(position).getId_junta();
        } catch (Exception e) {
            idPropietario = 0 ;
            e.printStackTrace();
        }
        return  idPropietario;
    }

    @Override
    public PropietarioViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.esqueleto_propietarios, viewGroup, false);
        return new PropietarioViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PropietarioViewHolder viewHolder, int i) {
        try {
            String nombreCompleto = items.get(i).getNombre() +""+ items.get(i).getApellido() ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        //viewHolder.imagen.setImageResource( );
        viewHolder.nombre.setText(items.get(i).getNombre() +" "+ items.get(i).getApellido());
        viewHolder.Apartamento.setText("Edf. "+items.get(i).getNombre_edificio() +" Apt."+ items.get(i).getNumero_vivienda());
        viewHolder.cedula.setText("Ci. "+items.get(i).getCedula());
    }

    public void filterPropietario(String text) {

        items = new ArrayList<>();

        if(text.isEmpty()){
            items.addAll(itemsRespaldo);

        } else{
            //toLowerCase Convierte todos los caracteres de la cadena a minúsculas.
            text = text.toLowerCase();

            String descripcionDetallada = "";
            String descripcion = "";

            for (int position = 0 ; position < itemsRespaldo.size() ; position++){

                System.out.println("Entro en el adapatodor entro en el for");

                descripcion = (itemsRespaldo.get(position).getCedula());
                descripcionDetallada = (itemsRespaldo.get(position).getNombre());

                // Si la desccripcion detallada o la normal contienen el query añadelo a la lista
                if (!descripcion.isEmpty() && descripcion.toLowerCase().contains(text)){

                    items.add(itemsRespaldo.get(position));

                }

                if (!descripcionDetallada.isEmpty() && descripcionDetallada.toLowerCase().contains(text)){

                    items.add(itemsRespaldo.get(position));

                }
            }


        }
        notifyDataSetChanged();
    }


}