package condominet.com.antares.Modelo;

/**
 * Created by Aqurdaneta on 01-07-2017.
 */

public class mensajeObj {
    String titulo = "";
    String mensaje = "";
    int tipo = -1;

    public mensajeObj(String titulo, String mensaje, int tipo){
        this.titulo = titulo;
        this.mensaje = mensaje;
        this.tipo = tipo;
    }

    public int getTipo() {
        return tipo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public String getTitulo() {
        return titulo;
    }
}
