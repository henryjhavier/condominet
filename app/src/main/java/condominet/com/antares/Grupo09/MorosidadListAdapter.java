package condominet.com.antares.Grupo09;

import android.app.ActivityManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import condominet.com.antares.Modelo.Morosidad;
import condominet.com.antares.Modelo.Propietario;
import condominet.com.antares.R;

/**
 * Created by Daniel13 on 6/24/2017.
 */

public class MorosidadListAdapter extends ArrayAdapter<Morosidad> implements View.OnClickListener {

    ArrayList<Morosidad> dataSet;
    ArrayList<Morosidad> filterList;
    Context mContext;
    Filter filter;


    // View lookup cache
    private static class ViewHolder {
        TextView nombrePro;
        TextView apellidoPro;
        TextView cedulaPro;
        TextView apartamentoPro;
    }

    public MorosidadListAdapter(ArrayList<Morosidad> data, Context context) {
        super(context, R.layout.celda_morosidad, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View view) {

    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Morosidad morosidad = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        MorosidadListAdapter.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new MorosidadListAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.celda_morosidad, parent, false);
            viewHolder.nombrePro = (TextView) convertView.findViewById(R.id.nombre);
            viewHolder.apellidoPro = (TextView) convertView.findViewById(R.id.apellido);
            viewHolder.cedulaPro = (TextView) convertView.findViewById(R.id.ID);
            viewHolder.apartamentoPro = (TextView) convertView.findViewById(R.id.apartamento);


            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MorosidadListAdapter.ViewHolder) convertView.getTag();
            result=convertView;
        }

        // Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        //result.startAnimation(animation);
        //lastPosition = position;

        viewHolder.nombrePro.setText(morosidad.getNombre());
        viewHolder.apellidoPro.setText(morosidad.getApellido());
        viewHolder.cedulaPro.setText(morosidad.getCedula());
        viewHolder.apartamentoPro.setText(morosidad.getVivienda());
        // viewHolder.info.setOnClickListener(this);
        // viewHolder.info.setTag(position);
        // Return the completed view to render on screen


        return convertView;


    }

    @Override
    public Filter getFilter()
    {
        if(filter == null) {
            filter = new MorosidadListAdapter.CustomFilter();
        }
        return filter;
    }

    private class CustomFilter extends Filter
    {


        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // NOTE: this function is *always* called from a background thread, and
            // not the UI thread.
            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if(constraint != null && constraint.toString().length() > 0)
            {
                ArrayList<Morosidad> filt = new ArrayList<>();
                ArrayList<Morosidad> lItems = new ArrayList<>();
                synchronized (this)
                {
                    lItems.addAll(dataSet);
                }
                for(int i = 0, l = lItems.size(); i < l; i++)
                {
                    Morosidad morosidad = lItems.get(i);
                    if
                            (morosidad.getApellido().toLowerCase().contains(constraint) || morosidad.getCedula().contains(constraint) || morosidad.getVivienda().toLowerCase().contains(constraint))
                        filt.add(morosidad);
                }

                result.count = filt.size();
                result.values = filt;
            }
            else
            {


                synchronized(this)
                {

                    result.values = dataSet;
                    result.count = dataSet.size();

                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            // NOTE: this function is *always* called from the UI thread.

            filterList = (ArrayList<Morosidad>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0, l = filterList.size(); i < l; i++)
                add(filterList.get(i));
            notifyDataSetInvalidated();



        }

    }

}
