package condominet.com.antares.Modelo;

import java.io.Serializable;

/**
 * Created by Daniel13 on 6/14/2017.
 */

public class Evento implements Serializable {

    public String id;
    public String asunto;
    public String mensaje;
    public String automatica = "no";
    public String prioridad;

    public String fechaOcurrencia;
    public String horaOcurrencia;

    public Evento(String id,String asunto, String mensaje, String automatica, String prioridad, String fechaOcurrencia, String horaOcurrencia) {
        this.id = id;
        this.asunto = asunto;
        this.mensaje = mensaje;
        this.automatica = automatica;
        this.prioridad = prioridad;
        this.fechaOcurrencia = fechaOcurrencia;
        this.horaOcurrencia = horaOcurrencia;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }


    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getFechaOcurrencia() {
        return fechaOcurrencia;
    }

    public void setFechaOcurrencia(String fechaOcurrencia) {
        this.fechaOcurrencia = fechaOcurrencia;
    }

    public String getHoraOcurrencia() {
        return horaOcurrencia;
    }

    public void setHoraOcurrencia(String horaOcurrencia) {
        this.horaOcurrencia = horaOcurrencia;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
