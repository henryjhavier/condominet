package condominet.com.antares.Grupo12.Activades;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import condominet.com.antares.Grupo12.ConstantesURL;
import condominet.com.antares.Grupo12.Modoles.Propietario;
import condominet.com.antares.R;

import java.lang.reflect.Type;
import java.util.ArrayList;

import condominet.com.antares.Grupo12.Adaptadores.AdaptadorPropietarios;
import condominet.com.antares.RecyclerItemClickListener;

public class Consultar_recibos_pendientes extends AppCompatActivity implements AdapterView.OnItemSelectedListener,   SearchView.OnQueryTextListener{



    // URL of object to be parsed
    final String JsonURL     = ConstantesURL.uRL_BASE+"/kindles.json";
    final String JsonURLPost = ConstantesURL.uRL_BASE+"/v1/users/";
    String JsonURLParaLosPropietarios = ConstantesURL.uRL_BASE+"/v1/owners/1";
    final String JsonURLParaLosInmuebles = ConstantesURL.uRL_BASE+"/v1/inmuebles/1";
    String JsonURLParaLosRecibos = "";
    // Defining the Volley request queue that handles the URL request concurrently
    RequestQueue requestQueue;
    private RecyclerView recycler;
    private  AdaptadorPropietarios adapterRV;
    private RecyclerView.LayoutManager lManager;
    private RelativeLayout rl_sin_resultados;
    private Intent intent;
    Spinner spinner;
    ProgressBar progresoRequest;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_recibos_pendientes);
        System.out.println("Resultados");

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        requestQueue = Volley.newRequestQueue(getApplicationContext());

        spinner = (Spinner) findViewById(R.id.sp_inmuebles);
        rl_sin_resultados = (RelativeLayout) findViewById(R.id.rl_no_hay_resultados_busqueda_propietario);
        progresoRequest = (ProgressBar) findViewById(R.id.indeterminateBar);
        // Obtener el Recycler

        spinner.setOnItemSelectedListener(this);

        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);



        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getApplicationContext());
        recycler.setLayoutManager(lManager);



        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(this, R.array.arreglo_prueba,R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);


        requestQueue = Volley.newRequestQueue(getApplicationContext());



        //
        //   datosCableados();



        recycler.addOnItemTouchListener(
                new RecyclerItemClickListener(getBaseContext(), recycler ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {

                        // do whatever
                        int idPropietario = adapterRV.getPropietarioIdVivienda(position);
                        int idVivienda = adapterRV.getPropietarioIdVivienda(position);
                        int idJunta = adapterRV.getPropietarioIdJunta(position);

                        ConstantesURL.uRL_BASE = "http://"+ConstantesURL.iP+":3000/api" ;
                        ConstantesURL.JsonURL_Base_ParaLosRecibos = ConstantesURL.uRL_BASE+"/v1/recibos/";

                        JsonURLParaLosRecibos = ConstantesURL.JsonURL_Base_ParaLosRecibos+""+idPropietario;
                        System.out.println("URL"+JsonURLParaLosRecibos);

                        ConstantesURL.JsonURLParaLosRecibos = JsonURLParaLosRecibos;

                        intent = new Intent(Consultar_recibos_pendientes.this, Pagar_recibos.class);

                        intent.putExtra("id_vivienda",idVivienda);
                        intent.putExtra("id_junta",idJunta);


                        startActivity(intent);
                        //  Toast.makeText(getBaseContext(), "Single Click on position :"+position, Toast.LENGTH_SHORT).show();
                        //  Toast.makeText(getBaseContext(), "URL :"+JsonURLParaLosRecibos, Toast.LENGTH_SHORT).show();
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );



    }

/*
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_propietarios_henry, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem mSearchmenuItem = menu.findItem(R.id.searchItem);
        SearchView searchView = (SearchView) mSearchmenuItem.getActionView();
        searchView.setQueryHint("Buscar...");
        searchView.setOnQueryTextListener(this);
        return true;
*/
    public JSONObject pasarParametro() {
        JSONObject param = new JSONObject();
        JSONArray criteria_e = new JSONArray();
        JSONObject param_obj = new JSONObject();
        //agregamos los parametros al JSONOBJECT
        try {
            //param_obj.put("email", "calonzo@gmail.com");
            param_obj.put("email", "Daniel");
            param_obj.put("password", "1234");
            //  param_obj.put("password", "123456");


            criteria_e.put(param_obj);
            param.put("user", param_obj);
            //VolleyLog.d(TAG, "Error: " +param.toString().getBytes("utf-8") );

        } catch (Exception e) {
        }
        return param;
    }


    public void datosCableados(){

// cableado

        ArrayList<Propietario> propietario = null;
        propietario = new ArrayList<>();

        propietario.add(new Propietario(1, 1, 1, "23691111", "Henry", "Rojas", "fechaNacimiento", "telefonoLocal",
                "celular1", "celular2", "hjro@gmail.com", "Edf Cumbre", "3B"));

        propietario.add(new Propietario(1, 1, 1, "23712084", "Sabina", "Qui", "fechaNacimiento", "telefonoLocal",
                "celular1", "celular2", "Sabina@gmail.com", "Edf Maracapana", "1A"));

        propietario.add(new Propietario(1, 1, 1 ,"23712712", "Alejandro", "felipe", "fechaNacimiento", "telefonoLocal",
                "celular1", "celular2", "Alejandro@gmail.com", "Edf Cumbre", "1B"));

        propietario.add(new Propietario(1, 1, 1 ,"11111111", "Leydi", "Obando", "fechaNacimiento", "telefonoLocal",
                "celular1", "celular2", "Leydi@gmail.com", "Edf Mis lalos", "C"));

        propietario.add(new Propietario(1, 1,  1,"3333333", "David", "Rojas", "fechaNacimiento", "telefonoLocal",
                "celular1", "celular2", "hjro@gmail.com", "Edf Prueba", "AA"));

        propietario.add(new Propietario(1, 1, 1 ,"45454545", "Henry", "Rojas", "fechaNacimiento", "telefonoLocal",
                "celular1", "celular2", "david@gmail.com", "Edf Cumbre", "3B"));

        propietario.add(new Propietario(1,1, 1  ,"23692111", "Henry2", "Rojas2", "fechaNacimiento", "telefonoLocal",
                "celular1", "celular2", "hjro2@gmail.com", "Edf Cumbre", "3B"));



        adapterRV = new AdaptadorPropietarios(propietario);

        recycler.setAdapter(adapterRV);

        // cableado

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        System.out.println("Esto es lo que esta dentro el spinner " +adapterView.getItemAtPosition(i).toString());


        String numero = adapterView.getItemAtPosition(i).toString();

        final String movistar1 = "Inmueble 1";
        final String movistar2 = "Inmueble 2";
        final String movilnet1 = "Inmueble 3";
        final String movilnet2 = "Inmueble 4";


        switch(numero) {
            case movistar1:
                //Toast.makeText(getBaseContext(), "Single Click on position :"+adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();
                JsonURLParaLosPropietarios = ConstantesURL.uRL_BASE+"/v1/owners/"+"1";
                ConstantesURL.id_inmueble = "1";
                break;
            case movistar2:
                //Toast.makeText(getBaseContext(), "Single Click on position :"+adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();
                JsonURLParaLosPropietarios = ConstantesURL.uRL_BASE+"/v1/owners/"+"2";

                ConstantesURL.id_inmueble = "2";

                break;
            case movilnet1:
                //Toast.makeText(getBaseContext(), "Single Click on position :"+adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();
                JsonURLParaLosPropietarios = ConstantesURL.uRL_BASE+"/v1/owners/"+"3";
                ConstantesURL.id_inmueble = "3";

                break;

            case movilnet2:
                //Toast.makeText(getBaseContext(), "Single Click on position :"+adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();
                JsonURLParaLosPropietarios = ConstantesURL.uRL_BASE+"/v1/owners/"+"4";

                break;


            default:
        }

        seleccionaInmueble();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    public void seleccionaInmueble(){

        progresoRequest.setVisibility(View.VISIBLE);

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, JsonURLParaLosPropietarios,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        Gson gson = new GsonBuilder().disableHtmlEscaping().serializeNulls().create();
                        ArrayList<Propietario> propietario = null;

                        try {
                            Type listType = new TypeToken<ArrayList<Propietario>>(){}.getType();

                            propietario = gson.fromJson(response.toString(), listType);

                            adapterRV = new AdaptadorPropietarios(propietario);

                            recycler.setAdapter(adapterRV);

                            progresoRequest.setVisibility(View.GONE);


                            if ( propietario.size() == 0 ){

                                rl_sin_resultados.setVisibility(View.VISIBLE);
                            }else
                                rl_sin_resultados.setVisibility(View.GONE);


                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Resultados 4"+ propietario.size());

                        for (int i = 0; i < propietario.size(); i++) {

                            System.out.println("Resultados nombre "+propietario.get(i).getNombre());

                        }

                    }





                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error.Response", "" + error);
                        progresoRequest.setVisibility(View.GONE);
                        Toast.makeText(getBaseContext(), "No se pudo comunicar con el servidor" , Toast.LENGTH_SHORT).show();


                    }
                }
        );
// add it to the RequestQueue
        requestQueue.add(getRequest);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {


        if ( adapterRV.getItemCountRespaldo() > 0 ) {
            //  vacio.setVisibility(View.GONE);

            adapterRV.filterPropietario(newText);

        }


        if ( adapterRV.getItemCount() == 0 ){

            rl_sin_resultados.setVisibility(View.VISIBLE);
        }else
            rl_sin_resultados.setVisibility(View.GONE);

        return false;
    }
}