package condominet.com.antares.Grupo09;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import condominet.com.antares.R;

/**
 * Created by Barbara on 01/07/2017.
 */

public class Detalle_morosidad extends AppCompatActivity {

    TextView tvnombre, tvapelli, tvced, tvapto, tvlocal, tvmeses, tvcantidad, tvcorreo;

    @Override
    protected void onCreate(Bundle savedInstancesState) {
        super.onCreate(savedInstancesState);
        setContentView(R.layout.activity_detalle_morosidad);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_DetalleMorosidad);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        String nombre= intent.getStringExtra("nombre");
      //  String apellido= intent.getStringExtra("apellido");
        String cedula= intent.getStringExtra("cedula");
        String telefono= intent.getStringExtra("telefono");
        String correo= intent.getStringExtra("correo");
      //  String edificio= intent.getStringExtra("edificio");
        String vivienda= intent.getStringExtra("vivienda");
        String meses= intent.getStringExtra("meses");
        String deuda= intent.getStringExtra("deuda");

        tvnombre = (TextView) findViewById(R.id.tv_NombrePro);
     //   tvapelli = (TextView) findViewById(R.id.tv_ApelliPro);
        tvced = (TextView) findViewById(R.id.tv_CedulaPro);
        tvlocal = (TextView) findViewById(R.id.tv_TelfLocal);
        tvcorreo = (TextView) findViewById(R.id.tv_correoEl);
       // tvedif = (TextView) findViewById(R.id.tv_EdifPro);
        tvapto = (TextView) findViewById(R.id.tv_AptoProp);
        tvmeses = (TextView) findViewById(R.id.tv_MesesPorPagar);
        tvcantidad = (TextView) findViewById(R.id.tv_MontoTotal);

        tvnombre.setText(nombre);
        tvced.setText(cedula);
        tvlocal.setText(telefono);
        tvcorreo.setText(correo);
      //  tvedif.setText(edificio);
        tvapto.setText(vivienda);
        tvcantidad.setText(deuda);
        tvmeses.setText(meses);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
