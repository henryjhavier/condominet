package condominet.com.antares.Grupo09;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

import condominet.com.antares.Modelo.Recibo;
import condominet.com.antares.R;

/**
 * Created by Barbara on 25/06/2017.
 */

public class Recibos_pendientes extends AppCompatActivity {

    ReciboListAdapter adapter;
    Toolbar tbMainSearch;

    ArrayList<Recibo> listaRecibos;
    ListView lvTollbarSearch;


    @Override
    protected void onCreate(Bundle savedInstancesState) {
        super.onCreate(savedInstancesState);
        setContentView(R.layout.activity_recibos_pendientes);
        setUpViews();

        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbarRec);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }


    private void setUpViews() {

        tbMainSearch = (Toolbar) findViewById(R.id.tb_toolbarRec);
        lvTollbarSearch = (ListView) findViewById(R.id.lv_toolbarSearch);

        listaRecibos = (ArrayList<Recibo>) getIntent().getSerializableExtra("ListaRecibos");

        adapter = new ReciboListAdapter(listaRecibos,getApplicationContext());
        lvTollbarSearch.setAdapter(adapter);

        setSupportActionBar(tbMainSearch);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_propietario,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}