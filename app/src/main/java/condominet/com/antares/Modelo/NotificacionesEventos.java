package condominet.com.antares.Modelo;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Daniel13 on 6/14/2017.
 */

public class NotificacionesEventos {

    public String fechaOcurrencia;
    public String horaOcurrencia;
    public String descripcion;
    public ArrayList<String> prioridad;
    public String asunto;

    public NotificacionesEventos(String fechaOcurrencia, String horaOcurrencia, String descripcion, ArrayList<String> prioridad, String asunto) {
        this.fechaOcurrencia = fechaOcurrencia;
        this.horaOcurrencia = horaOcurrencia;
        this.descripcion = descripcion;
        this.prioridad = prioridad;
        this.asunto = asunto;
    }

    public String getFechaOcurrencia() {
        return fechaOcurrencia;
    }

    public void setFechaOcurrencia(String fechaOcurrencia) {
        this.fechaOcurrencia = fechaOcurrencia;
    }

    public String getHoraOcurrencia() {
        return horaOcurrencia;
    }

    public void setHoraOcurrencia(String horaOcurrencia) {
        this.horaOcurrencia = horaOcurrencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<String> getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(ArrayList<String> prioridad) {
        this.prioridad = prioridad;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }
}
