package condominet.com.antares.Modelo;


import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Daniel13 on 6/19/2017.
 */

public class Prueba  {

//    @SerializedName("models")
//    ArrayList<Models> models;

    @Nullable
    @SerializedName("owners")
    ArrayList<Propietario> propietarios;

    @Nullable
    @SerializedName("moroidades")
    ArrayList<Morosidad> morosidades;

    @Nullable
    @SerializedName("alerts")
    ArrayList<Evento> notificaciones;

    @Nullable
    @SerializedName("id")
    String idEvento;

    @Nullable
    @SerializedName("recibos")
    ArrayList<Recibo> recibos;

    public Prueba(ArrayList<Evento> notificaciones) {
        this.notificaciones = notificaciones;
    }

    @Nullable
    public ArrayList<Evento> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(@Nullable ArrayList<Evento> notificaciones) {
        this.notificaciones = notificaciones;
    }

    public ArrayList<Propietario> getPropietarios() {
        return propietarios;
    }

    public void setPropietarios(ArrayList<Propietario> propietarios) {
        this.propietarios = propietarios;
    }

    public ArrayList<Recibo> getRecibos() {
        return recibos;
    }

    public void setRecibos(ArrayList<Recibo> recibos) {
        this.recibos = recibos;
    }

//    public ArrayList<Models> getModels() {
//        return models;
//    }

//    public void setModels(ArrayList<Models> models) {
//        this.models = models;
//    }

    public ArrayList<Morosidad> getMorosidades() {
        return morosidades;
    }

    public void setMorosidades(ArrayList<Morosidad> morosidades) {
        this.morosidades = morosidades;
    }


}
