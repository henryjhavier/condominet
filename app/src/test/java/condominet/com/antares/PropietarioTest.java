package condominet.com.antares;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;

import condominet.com.antares.Grupo12.Activades.Consultar_recibos_pendientes;
import condominet.com.antares.Grupo12.Modoles.Propietario;
import condominet.com.antares.Grupo12.Modoles.Recibos;

import static org.junit.Assert.*;

/**
 * Created by h3nry on 3/7/2017.
 */
public class PropietarioTest {
    ArrayList<Propietario> propietario;

    public PropietarioTest() {

        propietario = null;

        propietario = new ArrayList<>();

        propietario.add(new Propietario(1, 1, 1, "23691111", "Henry", "Rojas", "fechaNacimiento", "telefonoLocal", "celular1", "celular2", "hjro@gmail.com", "Edf Cumbre", "3B"));

        propietario.add(new Propietario(1, 1, 1, "23712084", "Sabina", "Qui", "fechaNacimiento", "telefonoLocal",   "celular1", "celular2", "Sabina@gmail.com", "Edf Maracapana", "1A"));

        propietario.add(new Propietario(1, 1 ,1 ,null , null , null , null, null ,null,null ,null , null ,null ));

    }


    @Test
    public void testHayPropietarios() {

        Assert.assertTrue("Hay Propietarios", propietario.size() > 0);
    }


    ///Tes para filtrador con exito
    @Test
    public void tesFiltraPropietarios() {

        String textoParaFiltrar = "enry";
        String buscarPorNombre;
        //toLowerCase Convierte todos los caracteres de la cadena a minúsculas.
        textoParaFiltrar = textoParaFiltrar.toLowerCase();

        buscarPorNombre = (propietario.get(0).getNombre());

        Assert.assertTrue("Encontro conincidencia",buscarPorNombre.toLowerCase().contains(textoParaFiltrar));

    }


    //Test para falla de filtrado
    @Test
    public void tesNoPudoFiltraPropietarios() {

        String textoParaFiltrar = "enry";

        textoParaFiltrar = textoParaFiltrar.toLowerCase();

        String buscarPorNombre = (propietario.get(1).getNombre());

        Assert.assertFalse("Sin resultados.",buscarPorNombre.toLowerCase().contains(textoParaFiltrar));

    }

    //Test para crear structuras Json
    @Test
    public void testCrearJsonObject() {

        Consultar_recibos_pendientes crearObjeto = new Consultar_recibos_pendientes();

        JSONObject crearJson = crearObjeto.pasarParametro();

        Assert.assertNotNull(crearJson);
    }

    @Test
    public void buscaPropietario() throws  NullPointerException{

        String  idPropietario = propietario.get(2).getApellido();

        Assert.assertNull(idPropietario);

    }

    @Test
    public void tamañoItems() throws  NullPointerException{

        String  idPropietario = propietario.get(1).getApellido();

        Assert.assertNotNull(idPropietario);

    }






}