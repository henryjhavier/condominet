package condominet.com.antares.Grupo12.Adaptadores;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import condominet.com.antares.Grupo12.Modoles.Solicitud_servicios;
import condominet.com.antares.R;

/**
 * Created by h3nry on 21/6/2017.
 */


public class AdaptadorSolicitudServicio extends RecyclerView.Adapter<AdaptadorSolicitudServicio.Solicitud_serviciosViewHolder> {
    private ArrayList<Solicitud_servicios> items;

    public static class Solicitud_serviciosViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item

        public TextView nombe;
        public TextView cedula;
        public TextView tipoServicio;
        public TextView numeroApartamento;

        public Solicitud_serviciosViewHolder(View v) {
            super(v);
            nombe = (TextView) v.findViewById(R.id.tv_Nombre);
            cedula = (TextView) v.findViewById(R.id.tv_Cedul);
            tipoServicio = (TextView) v.findViewById(R.id.tv_tipo_servicio);
            numeroApartamento = (TextView) v.findViewById(R.id.tv_numero_apartamento);

            this.setIsRecyclable (false);

        }
    }

    public AdaptadorSolicitudServicio(ArrayList<Solicitud_servicios> items) {
        this.items = items;
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public Solicitud_serviciosViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.esqueleto_solicitud_servicios, viewGroup, false);
        return new Solicitud_serviciosViewHolder(v);
    }

    @Override
    public void onBindViewHolder(Solicitud_serviciosViewHolder viewHolder, int i) {


        viewHolder.nombe.setText("Nombre: "+items.get(i).getNombre()+" "+items.get(i).getApellido_propietario());
        viewHolder.cedula.setText("Cedula:"+ items.get(i).getNombre_propietario());
        viewHolder.tipoServicio.setText("Tipo Servicio: "+items.get(i).getTipo_servicio());
        viewHolder.numeroApartamento.setText("Nro. Apartamento"+items.get(i).getNumero_vivienda());



    }
}